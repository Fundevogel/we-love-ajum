import filecmp

from click.testing import CliRunner

from ajum.cli import cli


# Global setup
# (1) Initialize CLI runner
runner = CliRunner()

# (2) Test config
config = '-c tests/fixtures/config.json'


def test_cli():
    # Run function
    result = runner.invoke(cli)

    # Assert result
    assert result.exit_code == 0


def test_export():
    # Setup
    # (1) Output file
    fixture = 'tests/fixtures/results/index.json'

    # (2) Expected output
    expected = 'tests/fixtures/expected/index.json'

    # Run function
    result = runner.invoke(cli, '{} export {}'.format(config, fixture))

    # Assert result
    # (1) Exit code
    assert result.exit_code == 0

    # (2) File comparison
    assert filecmp.cmp(fixture, expected)


def test_export_strict():
    # Setup
    # (1) Output file
    fixture = 'tests/fixtures/results/strict.json'

    # (2) Expected output
    expected = 'tests/fixtures/expected/strict.json'

    # Run function
    result = runner.invoke(cli, '{} export -s {}'.format(config, fixture))

    # Assert result
    # (1) Exit code
    assert result.exit_code == 0

    # (2) File comparison
    assert filecmp.cmp(fixture, expected)


def test_export_full():
    # Setup
    # (1) Output file
    fixture = 'tests/fixtures/results/full.json'

    # (2) Expected output
    expected = 'tests/fixtures/expected/full.json'

    # Run function
    result = runner.invoke(cli, '{} export -f {}'.format(config, fixture))

    # Assert result
    # (1) Exit code
    assert result.exit_code == 0

    # (2) File comparison
    assert filecmp.cmp(fixture, expected)


def test_export_both():
    # Setup
    # (1) Output file
    fixture = 'tests/fixtures/results/both.json'

    # (2) Expected output
    expected = 'tests/fixtures/expected/both.json'

    # Run function
    result = runner.invoke(cli, '{} export -s -f {}'.format(config, fixture))

    # Assert result
    # (1) Exit code
    assert result.exit_code == 0

    # (2) File comparison
    assert filecmp.cmp(fixture, expected)


def test_stats():
    # Run function
    result = runner.invoke(cli, 'stats')

    # Assert result
    assert result.exit_code == 0
